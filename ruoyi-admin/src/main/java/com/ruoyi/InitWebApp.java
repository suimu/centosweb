package com.ruoyi;

import com.ruoyi.ma.domain.ShellUser;
import com.ruoyi.ma.domain.ShellUserCache;
import com.ruoyi.ma.service.IShellUserService;
import com.ruoyi.ma.utils.InitAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 系统初始化需要执行的代码
 *
 * @author cj
 */
@Component
public class InitWebApp implements ApplicationRunner {

    @Autowired
    InitAppService initAppService;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("初始化读取参数");
        initAppService.initShellUserCache();
    }
}
