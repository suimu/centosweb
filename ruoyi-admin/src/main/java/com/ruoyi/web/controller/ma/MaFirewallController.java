package com.ruoyi.web.controller.ma;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.ma.domain.MaFirewall;
import com.ruoyi.ma.service.IMaFirewallService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 端口Controller
 *
 * @author cj
 * @date 2021-07-20
 */
@Controller
@RequestMapping("/ma/firewall")
public class MaFirewallController extends BaseController
{
    private String prefix = "ma/firewall";

    @Autowired
    private IMaFirewallService maFirewallService;

    @RequiresPermissions("ma:firewall:view")
    @GetMapping()
    public String firewall()
    {
        return prefix + "/firewall";
    }

    /**
     * 查询端口列表
     */
    @RequiresPermissions("ma:firewall:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MaFirewall maFirewall)
    {
        maFirewallService.updateTable();
        startPage();
        List<MaFirewall> list = maFirewallService.selectMaFirewallList(maFirewall);
        return getDataTable(list);
    }

    /**
     * 导出端口列表
     */
    @RequiresPermissions("ma:firewall:export")
    @Log(title = "端口", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MaFirewall maFirewall)
    {
        List<MaFirewall> list = maFirewallService.selectMaFirewallList(maFirewall);
        ExcelUtil<MaFirewall> util = new ExcelUtil<MaFirewall>(MaFirewall.class);
        return util.exportExcel(list, "端口数据");
    }

    /**
     * 新增端口
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存端口
     */
    @RequiresPermissions("ma:firewall:add")
    @Log(title = "端口", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MaFirewall maFirewall)
    {
        return toAjax(maFirewallService.insertMaFirewall(maFirewall));
    }

    /**
     * 修改端口
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        MaFirewall maFirewall = maFirewallService.selectMaFirewallById(id);
        mmap.put("maFirewall", maFirewall);
        return prefix + "/edit";
    }

    /**
     * 修改保存端口
     */
    @RequiresPermissions("ma:firewall:edit")
    @Log(title = "端口", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MaFirewall maFirewall)
    {
        return toAjax(maFirewallService.updateMaFirewall(maFirewall));
    }

    /**
     * 删除端口
     */
    @RequiresPermissions("ma:firewall:remove")
    @Log(title = "端口", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(maFirewallService.deleteMaFirewallByIds(ids));
    }
}