package com.ruoyi.web.controller.ma;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.ma.domain.ShellUser;
import com.ruoyi.ma.service.IShellUserService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * shell用户Controller
 * 
 * @author suimu
 * @date 2021-09-23
 */
@Controller
@RequestMapping("/ma/shelluser")
public class ShellUserController extends BaseController
{
    private String prefix = "ma/shelluser";

    @Autowired
    private IShellUserService shellUserService;

    @RequiresPermissions("ma:shelluser:view")
    @GetMapping()
    public String shelluser()
    {
        return prefix + "/shelluser";
    }

    /**
     * 查询shell用户列表
     */
    @RequiresPermissions("ma:shelluser:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShellUser shellUser)
    {
        startPage();
        List<ShellUser> list = shellUserService.selectShellUserList(shellUser);
        return getDataTable(list);
    }

    /**
     * 导出shell用户列表
     */
    @RequiresPermissions("ma:shelluser:export")
    @Log(title = "shell用户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShellUser shellUser)
    {
        List<ShellUser> list = shellUserService.selectShellUserList(shellUser);
        ExcelUtil<ShellUser> util = new ExcelUtil<ShellUser>(ShellUser.class);
        return util.exportExcel(list, "shell用户数据");
    }

    /**
     * 新增shell用户
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存shell用户
     */
    @RequiresPermissions("ma:shelluser:add")
    @Log(title = "shell用户", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShellUser shellUser)
    {
        return toAjax(shellUserService.insertShellUser(shellUser));
    }

    /**
     * 修改shell用户
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ShellUser shellUser = shellUserService.selectShellUserById(id);
        mmap.put("shellUser", shellUser);
        return prefix + "/edit";
    }

    /**
     * 修改保存shell用户
     */
    @RequiresPermissions("ma:shelluser:edit")
    @Log(title = "shell用户", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShellUser shellUser)
    {
        return toAjax(shellUserService.updateShellUser(shellUser));
    }

    /**
     * 删除shell用户
     */
    @RequiresPermissions("ma:shelluser:remove")
    @Log(title = "shell用户", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shellUserService.deleteShellUserByIds(ids));
    }
}
