package com.ruoyi.web.controller.ma;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.ma.domain.WebhookGitee;
import com.ruoyi.ma.service.IWebhookGiteeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * hookmapController
 * 
 * @author ruoyi
 * @date 2021-09-23
 */
@Controller
@RequestMapping("/ma/hookmap")
public class WebhookGiteeController extends BaseController
{
    private String prefix = "ma/hookmap";

    @Autowired
    private IWebhookGiteeService webhookGiteeService;

    @RequiresPermissions("ma:hookmap:view")
    @GetMapping()
    public String hookmap()
    {
        return prefix + "/hookmap";
    }

    /**
     * 查询hookmap列表
     */
    @RequiresPermissions("ma:hookmap:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WebhookGitee webhookGitee)
    {
        startPage();
        List<WebhookGitee> list = webhookGiteeService.selectWebhookGiteeList(webhookGitee);
        return getDataTable(list);
    }

    /**
     * 导出hookmap列表
     */
    @RequiresPermissions("ma:hookmap:export")
    @Log(title = "hookmap", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WebhookGitee webhookGitee)
    {
        List<WebhookGitee> list = webhookGiteeService.selectWebhookGiteeList(webhookGitee);
        ExcelUtil<WebhookGitee> util = new ExcelUtil<WebhookGitee>(WebhookGitee.class);
        return util.exportExcel(list, "hookmap数据");
    }

    /**
     * 新增hookmap
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存hookmap
     */
    @RequiresPermissions("ma:hookmap:add")
    @Log(title = "hookmap", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WebhookGitee webhookGitee)
    {
        return toAjax(webhookGiteeService.insertWebhookGitee(webhookGitee));
    }

    /**
     * 修改hookmap
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        WebhookGitee webhookGitee = webhookGiteeService.selectWebhookGiteeById(id);
        mmap.put("webhookGitee", webhookGitee);
        return prefix + "/edit";
    }

    /**
     * 修改保存hookmap
     */
    @RequiresPermissions("ma:hookmap:edit")
    @Log(title = "hookmap", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WebhookGitee webhookGitee)
    {
        return toAjax(webhookGiteeService.updateWebhookGitee(webhookGitee));
    }

    /**
     * 删除hookmap
     */
    @RequiresPermissions("ma:hookmap:remove")
    @Log(title = "hookmap", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(webhookGiteeService.deleteWebhookGiteeByIds(ids));
    }
}
