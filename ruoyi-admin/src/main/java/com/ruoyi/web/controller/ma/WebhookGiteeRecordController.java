package com.ruoyi.web.controller.ma;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.ma.domain.WebhookGiteeRecord;
import com.ruoyi.ma.service.IWebhookGiteeRecordService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * hookrecordController
 * 
 * @author ruoyi
 * @date 2021-09-23
 */
@Controller
@RequestMapping("/ma/hookrecord")
public class WebhookGiteeRecordController extends BaseController
{
    private String prefix = "ma/hookrecord";

    @Autowired
    private IWebhookGiteeRecordService webhookGiteeRecordService;

    @RequiresPermissions("ma:hookrecord:view")
    @GetMapping()
    public String hookrecord()
    {
        return prefix + "/hookrecord";
    }

    /**
     * 查询hookrecord列表
     */
    @RequiresPermissions("ma:hookrecord:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WebhookGiteeRecord webhookGiteeRecord)
    {
        startPage();
        List<WebhookGiteeRecord> list = webhookGiteeRecordService.selectWebhookGiteeRecordList(webhookGiteeRecord);
        return getDataTable(list);
    }

    /**
     * 导出hookrecord列表
     */
    @RequiresPermissions("ma:hookrecord:export")
    @Log(title = "hookrecord", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WebhookGiteeRecord webhookGiteeRecord)
    {
        List<WebhookGiteeRecord> list = webhookGiteeRecordService.selectWebhookGiteeRecordList(webhookGiteeRecord);
        ExcelUtil<WebhookGiteeRecord> util = new ExcelUtil<WebhookGiteeRecord>(WebhookGiteeRecord.class);
        return util.exportExcel(list, "hookrecord数据");
    }

    /**
     * 新增hookrecord
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存hookrecord
     */
    @RequiresPermissions("ma:hookrecord:add")
    @Log(title = "hookrecord", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WebhookGiteeRecord webhookGiteeRecord)
    {
        return toAjax(webhookGiteeRecordService.insertWebhookGiteeRecord(webhookGiteeRecord));
    }

    /**
     * 修改hookrecord
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        WebhookGiteeRecord webhookGiteeRecord = webhookGiteeRecordService.selectWebhookGiteeRecordById(id);
        mmap.put("webhookGiteeRecord", webhookGiteeRecord);
        return prefix + "/edit";
    }

    /**
     * 修改保存hookrecord
     */
    @RequiresPermissions("ma:hookrecord:edit")
    @Log(title = "hookrecord", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WebhookGiteeRecord webhookGiteeRecord)
    {
        return toAjax(webhookGiteeRecordService.updateWebhookGiteeRecord(webhookGiteeRecord));
    }

    /**
     * 删除hookrecord
     */
    @RequiresPermissions("ma:hookrecord:remove")
    @Log(title = "hookrecord", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(webhookGiteeRecordService.deleteWebhookGiteeRecordByIds(ids));
    }
}
