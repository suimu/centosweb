package com.ruoyi.ma.service;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * com.ruoyi.ma.service.IMaFirewallService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IMaFirewallServiceTest {

    @Autowired
    IMaFirewallService maFirewallService;

    @org.junit.Test
    public void selectMaFirewallList() {
        System.out.println(maFirewallService.selectMaFirewallList(null));
    }
}