package com.ruoyi.ma.utils;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * com.ruoyi.ma.utils.ShellUtil
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ShellUtilTest {

    @Autowired
    ShellUtil shellUtil;

    @Test
    public void getCommandMsg() {
        String command = "firewall-cmd --zone=public --list-ports";
        String result = shellUtil.getCommandMsg(command).trim();
        shellUtil.getCommandMsg("cd /home && pwd && ls -l");
        shellUtil.getCommandMsg("cd /usr/local/nginx/html && pwd && ls -l && git pull origin master");
    }
}