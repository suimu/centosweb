package com.ruoyi.ma.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 端口对象 ma_firewall
 *
 * @author cj
 * @date 2021-07-20
 */
public class MaFirewall extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** key值 */
    @Excel(name = "key值")
    private String firewallKey;

    /** 端口 */
    @Excel(name = "端口")
    private String firewallPort;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    private Integer sort;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setFirewallKey(String firewallKey)
    {
        this.firewallKey = firewallKey;
    }

    public String getFirewallKey()
    {
        return firewallKey;
    }
    public void setFirewallPort(String firewallPort)
    {
        this.firewallPort = firewallPort;
    }

    public String getFirewallPort()
    {
        return firewallPort;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("firewallKey", getFirewallKey())
                .append("firewallPort", getFirewallPort())
                .append("type", getType())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}