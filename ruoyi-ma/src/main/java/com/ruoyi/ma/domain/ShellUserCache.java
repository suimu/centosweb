package com.ruoyi.ma.domain;


import org.springframework.stereotype.Component;



@Component
public class ShellUserCache {

    public ShellUserCache(ShellUser shellUser) {
        this.username = shellUser.getUsername();
        this.password = shellUser.getPassword();
        this.host = shellUser.getHost();
        this.port = Integer.parseInt(shellUser.getPort());
    }

    public ShellUserCache() {}

    private String username;


    private String password;


    private String host;


    private int port;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "ShellUser{" +
                "user='" + username + '\'' +
                ", password='" + password + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                '}';
    }
}
