package com.ruoyi.ma.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * hookmap对象 webhook_gitee
 * 
 * @author ruoyi
 * @date 2021-09-23
 */
public class WebhookGitee extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编码 */
    private String id;

    /** 仓库地址 */
    @Excel(name = "仓库地址")
    private String repository;

    /** 本地地址 */
    @Excel(name = "本地地址")
    private String localUrl;

    /** 排序 */
    @Excel(name = "排序")
    private String sort;

    /** 分支 */
    @Excel(name = "分支")
    private String branch;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setRepository(String repository) 
    {
        this.repository = repository;
    }

    public String getRepository() 
    {
        return repository;
    }
    public void setLocalUrl(String localUrl) 
    {
        this.localUrl = localUrl;
    }

    public String getLocalUrl() 
    {
        return localUrl;
    }
    public void setSort(String sort) 
    {
        this.sort = sort;
    }

    public String getSort() 
    {
        return sort;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("repository", getRepository())
            .append("localUrl", getLocalUrl())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("sort", getSort())
            .append("remark", getRemark())
            .toString();
    }
}
