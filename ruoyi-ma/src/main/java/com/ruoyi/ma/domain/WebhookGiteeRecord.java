package com.ruoyi.ma.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * hookrecord对象 webhook_gitee_record
 * 
 * @author ruoyi
 * @date 2021-09-23
 */
public class WebhookGiteeRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编码 */
    private String id;

    /** 请求内容 */
    @Excel(name = "请求内容")
    private String requestPayload;

    /** 请求时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "请求时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date requestTime;

    /** 本地地址 */
    @Excel(name = "本地地址")
    private String localUrl;

    /** 请求类型 */
    @Excel(name = "请求类型")
    private String requestType;

    private String token;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setRequestPayload(String requestPayload) 
    {
        this.requestPayload = requestPayload;
    }

    public String getRequestPayload() 
    {
        return requestPayload;
    }
    public void setRequestTime(Date requestTime) 
    {
        this.requestTime = requestTime;
    }

    public Date getRequestTime() 
    {
        return requestTime;
    }
    public void setLocalUrl(String localUrl) 
    {
        this.localUrl = localUrl;
    }

    public String getLocalUrl() 
    {
        return localUrl;
    }
    public void setRequestType(String requestType) 
    {
        this.requestType = requestType;
    }

    public String getRequestType() 
    {
        return requestType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("requestPayload", getRequestPayload())
            .append("requestTime", getRequestTime())
            .append("localUrl", getLocalUrl())
            .append("requestType", getRequestType())
            .toString();
    }
}
