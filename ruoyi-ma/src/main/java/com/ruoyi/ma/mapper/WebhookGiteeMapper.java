package com.ruoyi.ma.mapper;

import java.util.List;
import com.ruoyi.ma.domain.WebhookGitee;

/**
 * hookmapMapper接口
 * 
 * @author ruoyi
 * @date 2021-09-23
 */
public interface WebhookGiteeMapper 
{
    /**
     * 查询hookmap
     * 
     * @param id hookmapID
     * @return hookmap
     */
    public WebhookGitee selectWebhookGiteeById(String id);

    /**
     * 查询hookmap列表
     * 
     * @param webhookGitee hookmap
     * @return hookmap集合
     */
    public List<WebhookGitee> selectWebhookGiteeList(WebhookGitee webhookGitee);

    /**
     * 新增hookmap
     * 
     * @param webhookGitee hookmap
     * @return 结果
     */
    public int insertWebhookGitee(WebhookGitee webhookGitee);

    /**
     * 修改hookmap
     * 
     * @param webhookGitee hookmap
     * @return 结果
     */
    public int updateWebhookGitee(WebhookGitee webhookGitee);

    /**
     * 删除hookmap
     * 
     * @param id hookmapID
     * @return 结果
     */
    public int deleteWebhookGiteeById(String id);

    /**
     * 批量删除hookmap
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWebhookGiteeByIds(String[] ids);
}
