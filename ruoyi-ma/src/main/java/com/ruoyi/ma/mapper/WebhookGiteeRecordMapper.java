package com.ruoyi.ma.mapper;

import java.util.List;
import com.ruoyi.ma.domain.WebhookGiteeRecord;

/**
 * hookrecordMapper接口
 * 
 * @author ruoyi
 * @date 2021-09-23
 */
public interface WebhookGiteeRecordMapper 
{
    /**
     * 查询hookrecord
     * 
     * @param id hookrecordID
     * @return hookrecord
     */
    public WebhookGiteeRecord selectWebhookGiteeRecordById(String id);

    /**
     * 查询hookrecord列表
     * 
     * @param webhookGiteeRecord hookrecord
     * @return hookrecord集合
     */
    public List<WebhookGiteeRecord> selectWebhookGiteeRecordList(WebhookGiteeRecord webhookGiteeRecord);

    /**
     * 新增hookrecord
     * 
     * @param webhookGiteeRecord hookrecord
     * @return 结果
     */
    public int insertWebhookGiteeRecord(WebhookGiteeRecord webhookGiteeRecord);

    /**
     * 修改hookrecord
     * 
     * @param webhookGiteeRecord hookrecord
     * @return 结果
     */
    public int updateWebhookGiteeRecord(WebhookGiteeRecord webhookGiteeRecord);

    /**
     * 删除hookrecord
     * 
     * @param id hookrecordID
     * @return 结果
     */
    public int deleteWebhookGiteeRecordById(String id);

    /**
     * 批量删除hookrecord
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWebhookGiteeRecordByIds(String[] ids);
}
