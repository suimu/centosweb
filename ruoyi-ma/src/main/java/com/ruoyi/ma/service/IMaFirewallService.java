package com.ruoyi.ma.service;

import java.util.List;
import com.ruoyi.ma.domain.MaFirewall;

/**
 * 端口Service接口
 *
 * @author cj
 * @date 2021-07-20
 */
public interface IMaFirewallService
{
    /**
     * 查询端口
     *
     * @param id 端口ID
     * @return 端口
     */
    public MaFirewall selectMaFirewallById(Long id);

    public void updateTable();

    /**
     * 查询端口列表
     *
     * @param maFirewall 端口
     * @return 端口集合
     */
    public List<MaFirewall> selectMaFirewallList(MaFirewall maFirewall);

    /**
     * 新增端口
     *
     * @param maFirewall 端口
     * @return 结果
     */
    public int insertMaFirewall(MaFirewall maFirewall);

    /**
     * 修改端口
     *
     * @param maFirewall 端口
     * @return 结果
     */
    public int updateMaFirewall(MaFirewall maFirewall);

    /**
     * 批量删除端口
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMaFirewallByIds(String ids);

    /**
     * 删除端口信息
     *
     * @param id 端口ID
     * @return 结果
     */
    public int deleteMaFirewallById(Long id);
}