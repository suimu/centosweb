package com.ruoyi.ma.service;

import java.util.List;
import com.ruoyi.ma.domain.ShellUser;

/**
 * shell用户Service接口
 * 
 * @author suimu
 * @date 2021-09-23
 */
public interface IShellUserService 
{
    /**
     * 查询shell用户
     * 
     * @param id shell用户ID
     * @return shell用户
     */
    public ShellUser selectShellUserById(String id);

    /**
     * 查询shell用户列表
     * 
     * @param shellUser shell用户
     * @return shell用户集合
     */
    public List<ShellUser> selectShellUserList(ShellUser shellUser);

    /**
     * 新增shell用户
     * 
     * @param shellUser shell用户
     * @return 结果
     */
    public int insertShellUser(ShellUser shellUser);

    /**
     * 修改shell用户
     * 
     * @param shellUser shell用户
     * @return 结果
     */
    public int updateShellUser(ShellUser shellUser);

    /**
     * 批量删除shell用户
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShellUserByIds(String ids);

    /**
     * 删除shell用户信息
     * 
     * @param id shell用户ID
     * @return 结果
     */
    public int deleteShellUserById(String id);
}
