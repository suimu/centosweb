package com.ruoyi.ma.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.json.JSONObject;
import com.ruoyi.ma.domain.WebhookGitee;
import com.ruoyi.ma.domain.WebhookGiteeRecord;

/**
 * hookmapService接口
 * 
 * @author ruoyi
 * @date 2021-09-23
 */
public interface IWebhookGiteeService 
{
    /**
     * 查询hookmap
     * 
     * @param id hookmapID
     * @return hookmap
     */
    public WebhookGitee selectWebhookGiteeById(String id);


    public boolean hook(WebhookGiteeRecord webhookGiteeRecord, JSONObject payload);

    /**
     * 查询hookmap列表
     * 
     * @param webhookGitee hookmap
     * @return hookmap集合
     */
    public List<WebhookGitee> selectWebhookGiteeList(WebhookGitee webhookGitee);

    /**
     * 新增hookmap
     * 
     * @param webhookGitee hookmap
     * @return 结果
     */
    public int insertWebhookGitee(WebhookGitee webhookGitee);

    /**
     * 修改hookmap
     * 
     * @param webhookGitee hookmap
     * @return 结果
     */
    public int updateWebhookGitee(WebhookGitee webhookGitee);

    /**
     * 批量删除hookmap
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWebhookGiteeByIds(String ids);

    /**
     * 删除hookmap信息
     * 
     * @param id hookmapID
     * @return 结果
     */
    public int deleteWebhookGiteeById(String id);
}
