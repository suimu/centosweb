package com.ruoyi.ma.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.ma.utils.ShellUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ma.mapper.MaFirewallMapper;
import com.ruoyi.ma.domain.MaFirewall;
import com.ruoyi.ma.service.IMaFirewallService;
import com.ruoyi.common.core.text.Convert;

/**
 * 端口Service业务层处理
 *
 * @author cj
 * @date 2021-07-20
 */
@Service
@Slf4j
public class MaFirewallServiceImpl implements IMaFirewallService {

    @Autowired
    private MaFirewallMapper maFirewallMapper;

    @Autowired
    private ShellUtil shellUtil;

    /**
     * 查询端口
     *
     * @param id 端口ID
     * @return 端口
     */
    @Override
    public MaFirewall selectMaFirewallById(Long id) {
        return maFirewallMapper.selectMaFirewallById(id);
    }


    @Override
    public void updateTable(){
        List<MaFirewall> firewallList = maFirewallMapper.selectMaFirewallList(null);
        Map<String, MaFirewall> firewallMap = firewallList.stream().collect(Collectors.toMap( MaFirewall::getFirewallKey, firewall -> firewall));
        String command = "firewall-cmd --zone=public --list-ports";
        String result = shellUtil.getCommandMsg(command).trim();
        if (!StringUtils.isEmpty(result)) {
            String[] resArr = result.split(" ");
            for (String item : resArr) {
                MaFirewall firewallItem = firewallMap.get(item);
                //用完删除，还有剩余的信息表示端口通过其他方式关闭，需要从表中删除
                firewallMap.remove(item);
                //为空表示没有这个端口，新增。
                if (firewallItem == null) {
                    String[] portInfo = item.split("/");
                    if (portInfo.length > 1) {
                        String port = portInfo[0];
                        String type = portInfo[1].contains("tcp") ? "0" : "1";
                        MaFirewall newPort = new MaFirewall();
                        newPort.setFirewallKey(item);
                        newPort.setFirewallPort(port);
                        newPort.setType(type);
                        newPort.setCreateTime(DateUtils.getNowDate());
                        newPort.setSort(0);
                        insertMaFirewall(newPort);
                    }
                }
            }
            //剩余的就是机器已经关闭的端口，但是表中未更新
            StringBuilder uselesskey = new StringBuilder("Delete port:  ");
            for (String key : firewallMap.keySet()) {
                Long id = firewallMap.get(key).getId();
                uselesskey.append(key).append("  ");
                deleteMaFirewallById(id);
            }
            log.info(uselesskey.toString());
        }
    }

    /**
     * 查询端口列表
     *
     * @param maFirewall 端口
     * @return 端口
     */
    @Override
    public List<MaFirewall> selectMaFirewallList(MaFirewall maFirewall) {
        return maFirewallMapper.selectMaFirewallList(maFirewall);
    }

    /**
     * 新增端口
     *
     * @param maFirewall 端口
     * @return 结果
     */
    @Override
    public int insertMaFirewall(MaFirewall maFirewall) {
        maFirewall.setCreateTime(DateUtils.getNowDate());
        return maFirewallMapper.insertMaFirewall(maFirewall);
    }

    /**
     * 修改端口
     *
     * @param maFirewall 端口
     * @return 结果
     */
    @Override
    public int updateMaFirewall(MaFirewall maFirewall) {
        maFirewall.setUpdateTime(DateUtils.getNowDate());
        return maFirewallMapper.updateMaFirewall(maFirewall);
    }

    /**
     * 删除端口对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMaFirewallByIds(String ids) {
        return maFirewallMapper.deleteMaFirewallByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除端口信息
     *
     * @param id 端口ID
     * @return 结果
     */
    @Override
    public int deleteMaFirewallById(Long id) {
        return maFirewallMapper.deleteMaFirewallById(id);
    }
}
