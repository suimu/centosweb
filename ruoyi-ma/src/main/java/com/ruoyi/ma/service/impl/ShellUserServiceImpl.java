package com.ruoyi.ma.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ma.mapper.ShellUserMapper;
import com.ruoyi.ma.domain.ShellUser;
import com.ruoyi.ma.service.IShellUserService;
import com.ruoyi.common.core.text.Convert;

/**
 * shell用户Service业务层处理
 * 
 * @author suimu
 * @date 2021-09-23
 */
@Service
public class ShellUserServiceImpl implements IShellUserService 
{
    @Autowired
    private ShellUserMapper shellUserMapper;

    /**
     * 查询shell用户
     * 
     * @param id shell用户ID
     * @return shell用户
     */
    @Override
    public ShellUser selectShellUserById(String id)
    {
        return shellUserMapper.selectShellUserById(id);
    }

    /**
     * 查询shell用户列表
     * 
     * @param shellUser shell用户
     * @return shell用户
     */
    @Override
    public List<ShellUser> selectShellUserList(ShellUser shellUser)
    {
        return shellUserMapper.selectShellUserList(shellUser);
    }

    /**
     * 新增shell用户
     * 
     * @param shellUser shell用户
     * @return 结果
     */
    @Override
    public int insertShellUser(ShellUser shellUser)
    {
        return shellUserMapper.insertShellUser(shellUser);
    }

    /**
     * 修改shell用户
     * 
     * @param shellUser shell用户
     * @return 结果
     */
    @Override
    public int updateShellUser(ShellUser shellUser)
    {
        return shellUserMapper.updateShellUser(shellUser);
    }

    /**
     * 删除shell用户对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShellUserByIds(String ids)
    {
        return shellUserMapper.deleteShellUserByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除shell用户信息
     * 
     * @param id shell用户ID
     * @return 结果
     */
    @Override
    public int deleteShellUserById(String id)
    {
        return shellUserMapper.deleteShellUserById(id);
    }
}
