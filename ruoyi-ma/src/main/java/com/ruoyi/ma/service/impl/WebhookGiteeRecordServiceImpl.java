package com.ruoyi.ma.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ma.mapper.WebhookGiteeRecordMapper;
import com.ruoyi.ma.domain.WebhookGiteeRecord;
import com.ruoyi.ma.service.IWebhookGiteeRecordService;
import com.ruoyi.common.core.text.Convert;

/**
 * hookrecordService业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-23
 */
@Service
public class WebhookGiteeRecordServiceImpl implements IWebhookGiteeRecordService 
{
    @Autowired
    private WebhookGiteeRecordMapper webhookGiteeRecordMapper;

    /**
     * 查询hookrecord
     * 
     * @param id hookrecordID
     * @return hookrecord
     */
    @Override
    public WebhookGiteeRecord selectWebhookGiteeRecordById(String id)
    {
        return webhookGiteeRecordMapper.selectWebhookGiteeRecordById(id);
    }

    /**
     * 查询hookrecord列表
     * 
     * @param webhookGiteeRecord hookrecord
     * @return hookrecord
     */
    @Override
    public List<WebhookGiteeRecord> selectWebhookGiteeRecordList(WebhookGiteeRecord webhookGiteeRecord)
    {
        return webhookGiteeRecordMapper.selectWebhookGiteeRecordList(webhookGiteeRecord);
    }

    /**
     * 新增hookrecord
     * 
     * @param webhookGiteeRecord hookrecord
     * @return 结果
     */
    @Override
    public int insertWebhookGiteeRecord(WebhookGiteeRecord webhookGiteeRecord)
    {
        return webhookGiteeRecordMapper.insertWebhookGiteeRecord(webhookGiteeRecord);
    }

    /**
     * 修改hookrecord
     * 
     * @param webhookGiteeRecord hookrecord
     * @return 结果
     */
    @Override
    public int updateWebhookGiteeRecord(WebhookGiteeRecord webhookGiteeRecord)
    {
        return webhookGiteeRecordMapper.updateWebhookGiteeRecord(webhookGiteeRecord);
    }

    /**
     * 删除hookrecord对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWebhookGiteeRecordByIds(String ids)
    {
        return webhookGiteeRecordMapper.deleteWebhookGiteeRecordByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除hookrecord信息
     * 
     * @param id hookrecordID
     * @return 结果
     */
    @Override
    public int deleteWebhookGiteeRecordById(String id)
    {
        return webhookGiteeRecordMapper.deleteWebhookGiteeRecordById(id);
    }
}
