package com.ruoyi.ma.service.impl;

import java.util.List;

import com.ruoyi.common.json.JSONObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.ma.domain.WebhookGiteeRecord;
import com.ruoyi.ma.service.IWebhookGiteeRecordService;
import com.ruoyi.ma.utils.ShellUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ma.mapper.WebhookGiteeMapper;
import com.ruoyi.ma.domain.WebhookGitee;
import com.ruoyi.ma.service.IWebhookGiteeService;
import com.ruoyi.common.core.text.Convert;

/**
 * hookmapService业务层处理
 *
 * @author ruoyi
 * @date 2021-09-23
 */
@Service
public class WebhookGiteeServiceImpl implements IWebhookGiteeService {

    @Autowired
    private WebhookGiteeMapper webhookGiteeMapper;

    @Autowired
    private IWebhookGiteeRecordService webhookGiteeRecordService;

    @Autowired
    ShellUtil shellUtil;

    /**
     * 查询hookmap
     *
     * @param id hookmapID
     * @return hookmap
     */
    @Override
    public WebhookGitee selectWebhookGiteeById(String id) {
        return webhookGiteeMapper.selectWebhookGiteeById(id);
    }

    @Override
    public boolean hook(WebhookGiteeRecord webhookGiteeRecord, JSONObject payload) {
        webhookGiteeRecord.setRequestPayload(payload.toString());
        // 获取仓库地址并查询详细信息
        JSONObject repository = (JSONObject) payload.get("repository");
        String gitHttpUrl = repository.get("git_http_url").toString();
        String key = getRepositoryUrlKey(gitHttpUrl);
        // 根据 key 查询匹配的仓库信息
        WebhookGitee query = new WebhookGitee();
        query.setRepository(key);
        List<WebhookGitee> webhookGiteeList = selectWebhookGiteeList(query);
        if (webhookGiteeList.size() == 1) {
            //有且只有一个，做相应的拉取操作
            WebhookGitee webhookGitee = webhookGiteeList.get(0);
            String localUrl = webhookGitee.getLocalUrl();
            webhookGiteeRecord.setLocalUrl(localUrl);
            String branch = webhookGitee.getBranch() != null ? webhookGitee.getBranch() : "master";
            String command = "cd " + localUrl + " && git pull origin " + branch;
            shellUtil.getCommandMsg(command);
            webhookGiteeRecordService.insertWebhookGiteeRecord(webhookGiteeRecord);
            // 获取
            return true;
        } else {
            String type = "fail :  " + webhookGiteeRecord.getRequestType();
            webhookGiteeRecord.setRequestType(type);
            webhookGiteeRecordService.insertWebhookGiteeRecord(webhookGiteeRecord);
            return false;
        }

    }

    private String getRepositoryUrlKey(String url) {
        String key = "";
        String noprefix = url.split("//")[1];
        String[] items = noprefix.split("\\.");
        String[] names = items[1].split("/");
        key += names[1];
        key += "/" + names[2];
        return key;
    }

    /**
     * 查询hookmap列表
     *
     * @param webhookGitee hookmap
     * @return hookmap
     */
    @Override
    public List<WebhookGitee> selectWebhookGiteeList(WebhookGitee webhookGitee) {
        return webhookGiteeMapper.selectWebhookGiteeList(webhookGitee);
    }

    /**
     * 新增hookmap
     *
     * @param webhookGitee hookmap
     * @return 结果
     */
    @Override
    public int insertWebhookGitee(WebhookGitee webhookGitee) {
        webhookGitee.setCreateTime(DateUtils.getNowDate());
        return webhookGiteeMapper.insertWebhookGitee(webhookGitee);
    }

    /**
     * 修改hookmap
     *
     * @param webhookGitee hookmap
     * @return 结果
     */
    @Override
    public int updateWebhookGitee(WebhookGitee webhookGitee) {
        webhookGitee.setUpdateTime(DateUtils.getNowDate());
        return webhookGiteeMapper.updateWebhookGitee(webhookGitee);
    }

    /**
     * 删除hookmap对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWebhookGiteeByIds(String ids) {
        return webhookGiteeMapper.deleteWebhookGiteeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除hookmap信息
     *
     * @param id hookmapID
     * @return 结果
     */
    @Override
    public int deleteWebhookGiteeById(String id) {
        return webhookGiteeMapper.deleteWebhookGiteeById(id);
    }
}
