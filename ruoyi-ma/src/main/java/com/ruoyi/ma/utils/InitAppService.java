package com.ruoyi.ma.utils;


import com.ruoyi.ma.domain.ShellUser;
import com.ruoyi.ma.domain.ShellUserCache;
import com.ruoyi.ma.service.IShellUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InitAppService {

    @Autowired
    IShellUserService shellUserService;

    @Autowired
    ShellUserCache shellUserCache;


    public void initShellUserCache() {
        List<ShellUser> users = shellUserService.selectShellUserList(null);
        if (users.size() > 0) {
            ShellUser shellUser = users.get(0);
            shellUserCache.setUsername(shellUser.getUsername());
            shellUserCache.setHost(shellUser.getHost());
            shellUserCache.setPort(Integer.parseInt(shellUser.getPort()));
            shellUserCache.setPassword(shellUser.getPassword());
        }

    }
}
